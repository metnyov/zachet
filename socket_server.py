import time
import socket

from queue import Queue


class SocketServer:

    def __init__(self, port):
        self.queue = Queue("localhost", port)

    def start(self):
        self.queue.start_server()
        while True:
            time.sleep(1)
            if self.queue.exists():
                self.handle(self.queue.get())

    def stop(self):
        self.queue.stop_server()

    def send(self, message, port):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(("localhost", port))
        try:
            sock.sendall(bytes(message, 'ascii'))
        finally:
            sock.close()

    def handle(self, message):
        pass