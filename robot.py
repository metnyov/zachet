import time
import random
import threading

from random import randint
from socket_server import SocketServer


SERVER_PORT = 8889
ROBOT_PORT = 8879

class Robot(SocketServer):

    def handle(self, message):
        msg = str(message)
        correct_msg = msg[2 : len(msg) - 1]
        if randint(1, 4) == 2:
            print(correct_msg + ": Success 25%")
            self.send(correct_msg + ":success", SERVER_PORT)
        else:
            print(correct_msg + ": failed 25%")


def start_server(robot):
    robot.start()
    robot.stop()


if __name__ == "__main__":
    robot = Robot(ROBOT_PORT)
    threading.Thread(target=start_server,args=(robot,)).start()
