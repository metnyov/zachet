import time
import threading

from random import randint
from socket_server import SocketServer


SERVER_PORT = 8889
ROBOT_PORT = 8879

class Server(SocketServer):

    def __init__(self, port):
        self.data_dict = {}
        self.wait_handle = False
        super().__init__(port)

    def start(self):
        self.queue.start_server()
        while True:
            time.sleep(1)

            if (self.wait_handle):
                continue
            
            update_time_keys = []
            del_keys = []
            for key, value in self.data_dict.items():
                if value > 0:
                    if (time.time() - value) > 5:
                        update_time_keys.append(key)
                        self.send(key, ROBOT_PORT)
                elif value == 0:
                    update_time_keys.append(key)
                    self.send(key, ROBOT_PORT)
                else:
                    del_keys.append(key)

            for key in update_time_keys:
                self.data_dict[key] = time.time()

            for key in del_keys:
                del self.data_dict[key]

            if self.queue.exists():
                self.handle(self.queue.get())

    def handle(self, message):
        msg = str(message)
        correct_msg = msg[2 : len(msg) - 1]
        if ("success" in correct_msg):
            data = correct_msg.split(":")[0]
            self.wait_handle = True
            self.data_dict[data] = -1
            self.wait_handle = False


def start_server(server):
    server.start()
    server.stop()


if __name__ == "__main__":
    server = Server(SERVER_PORT)

    threading.Thread(target=start_server,args=(server,)).start()

    while True:
        print("Message to robot:  ", end="")
        data = input()
        if data in server.data_dict.keys():
            print("Error: This message already in queue!")
        else:
            server.data_dict[data] = 0
